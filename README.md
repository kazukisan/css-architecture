# コーディングガイドライン

## Basic Rules
- キャメルケース・スネークケース禁止。単語の結合は `'-'(ハイフン)` のみとする。
- サイト全体で使う共通パーツには下記の接頭辞を付け、ユニークCSSの場合は接頭辞をつけない。
- CSS Preprocesserのネストは最大3階層まで。（極力2階層）
- `!impotant` は極力使わない

## Structure
Block, Element, Modifierから成る。

### Block
起点となる要素。
ex) `.block`

### Element
Blockを構成する要素。
Elementについては`block__element` のように `__` で記述する。
ex) `.block__element` 

### Modifier
BlockまたはElementのバリエーション違いの要素。
Modifierについては`block--modifier` のように `--` で記述する。
ex) `.block--type-example`, `.block__element--color-primary`



## Guide
|Layer|Prefix|Example|
|:----|:----|:----|
|Layout|`.l-*`|`.l-wrap`|
|Module|`.m-*`|`.m-card`|
|Component|`.c-*`|`.c-btn`|
|Utility|`.u-*`|`.u-img-flex`|
|State|`.is-*`|`.is-active`|
|Javascript|`.js-*`|`.js-modal-trigger`|


## Layout
ページの枠組みに関する要素


## Module
複数の要素からなるパーツ。
他の `Component`, `Module` を内包しても良い。


## Component
最小要素のパーツ。
1~3個程の要素から形成される。


## Utility
テキスト揃えや画像の伸縮等の汎用的なスタイルを付与するクラス。


## State
`is-active` など状態を表す


## Javascript
JSのトリガー、判別用クラス。